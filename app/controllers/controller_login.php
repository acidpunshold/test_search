<?php
class Controller_Login extends Controller
{
	
	function action_index()
	{
		if (isset($_SESSION['logged_user']))
			header('Location: /');
		$model = new Model_Login;
		if (isset($_POST['username']))
		{
			$model->username = $_POST['username'];
			$model->password = $_POST['password'];
			if ($model->validation())
			{
				$model->user = R::findOne('user', 'username = ?', array($model->username));
				if ( !empty($model->user) && ($model->login()) )
					header('Location: /');
				else
				{
					$model->errors['wrong'] = 'Неверное имя пользователя или пароль';
					return $this->view->generate('login_view.php', 'template_view.php', $model);
				}
			}else
				return $this->view->generate('login_view.php', 'template_view.php', $model);
		}
			return $this->view->generate('login_view.php', 'template_view.php', $model);		
	}

	
	function action_logout()
	{
		Model_Login::logout();
	}
}