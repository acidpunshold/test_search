<?php
class Controller_Search extends Controller
{
	function action_index()
	{
		$model = new Model_Search;

		if (isset($_GET['query']))
		{
			$model->query = $_GET['query'];
			$model->request = $model->getSearchRequest();
		}
		$this->view->generate('search_view.php', 'template_view.php', $model);
	}

}
