<?php
class Controller_Admin extends Controller
{
	function action_index(  )
	{	
		if ( isset($_SESSION['logged_user']) )
		{
			if ( $_SESSION['logged_user']->role == 'admin' )
			{
				
				return $this->view->generate('admin_view.php', 'template_view.php');
			}else
				header('Location: /');
		}
		else
			header('Location: /');
		
	}

	function action_users()
	{
		if ( isset($_SESSION['logged_user']) )
		{
			if ( $_SESSION['logged_user']->role == 'admin' )
			{
				$model = new Model_Admin;
				$page = explode('/', $_SERVER['REQUEST_URI']);
				if ( !empty($page[3]) )
					$model->page = $page[3];
				else 
					$model->page = 1;
				

				$model->find();
				return $this->view->generate('users_view.php', 'template_view.php', $model);
			}else
				header('Location: /');
		}
		else
			header('Location: /');
		
	}

	function action_delete ()
	{
		if ( isset($_SESSION['logged_user']) )
		{
			if ( $_SESSION['logged_user']->role == 'admin' )
			{
				$model = new Model_Admin;
				$id = explode('/', $_SERVER['REQUEST_URI']);
				$id = $id[3];
				$model->delete($id);
				header('Location: /admin/users/1');
				
			}else
				header('Location: /');
		}
		else
			header('Location: /');
	}

	function action_update ()
	{
		if ( isset($_SESSION['logged_user']) )
		{
			if ( $_SESSION['logged_user']->role == 'admin' )
			{

				$model = new Model_Admin;
				$id = explode('/', $_SERVER['REQUEST_URI']);
				$id = $id[3];
				$model->user = $model->getUser($id);

				if ( isset($_POST['username']) )
				{
					if ( $model->update($_POST) )
						header('Location: /admin/users/');
					else
						return $this->view->generate('upgrate_view.php', 'template_view.php', $model);
				}

				return $this->view->generate('upgrate_view.php', 'template_view.php', $model);
				
			}else
				header('Location: /');
		}
		else
			header('Location: /');
	}

	public function action_user()
	{
		if ( isset($_SESSION['logged_user']) )
		{
			if ( $_SESSION['logged_user']->role == 'admin' )
			{

				$model = new Model_Admin;
				$id = explode('/', $_SERVER['REQUEST_URI']);
				$id = $id[3];
				$model->user = $model->getUser($id);
				return $this->view->generate('user_view.php', 'template_view.php', $model->user);

				}else
				header('Location: /');
		}
		else
			header('Location: /');

	}

}