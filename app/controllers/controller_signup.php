<?php
class Controller_Signup extends Controller
{
	function action_index()
	{	
		if (isset($_SESSION['logged_user']))
			header('Location: /');
		$model = new Model_Signup;
		if (isset($_POST['username']))
		{
			$model->username = $_POST['username'];
			$model->password = $_POST['password'];
			$model->name = $_POST['name'];
			$model->lastname = $_POST['lastname'];
			$model->birth_date = $_POST['birth_date'];
			if ($model->validation())
			{
				
				$model->signup();
				header('Location: /login');
			}
			else
				return $this->view->generate('signup_view.php', 'template_view.php', $model);		
		}

		return $this->view->generate('signup_view.php', 'template_view.php', $model);
	}

}