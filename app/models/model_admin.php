<?php
class Model_Admin extends Model
{
	public $users;
	public $page;
	public $user;

	public $errors = array( 'username' => '', 'name' => '', 'lastname' => '', 'birth_date' => '');

	public function find()
	{

		$this->users = R::findAll( 'user', 'WHERE id > ? LIMIT 10', [$this->id()] );
	}

	public function id ()
	{
		$from = 0;
		if ( $this->page > 1 )
		{
			for ( $i = 1; $i < $this->page; $i++)
			{
				$from += 10;
			}
		}
		return $from;

	}

	public function delete ($id)
	{
		$user = R::load('user', $id);
		R::trash($user);
	}

	public function getUser ($id)
	{
		return R::load( 'user', $id );
	}

	public function validation()
	{
		$result = true;
		
		$this->errors = array( 'username' => '', 'name' => '', 'lastname' => '', 'birth_date' => '');
		
		if ( empty($this->user->username) )
		{
			$this->errors['username'] = 'Введите логин';
			$result = false;
		}
		
		if ( empty($this->user->name) )
		{
			$this->errors['name'] = 'Введите имя';
			$result = false;
		}
		if ( empty($this->user->lastname) )
		{
			$this->errors['lastname'] = 'Введите фамилию';
			$result = false;
		}
		if ( empty($this->user->birth_date) )
		{
			$this->errors['birth_date'] = 'Введите дату рождения';
			$result = false;
		}
		return $result;
	}

	public function update ($data)
	{
		$this->user->username = $data['username'];
		$this->user->name = $data['name'];
		$this->user->lastname = $data['lastname'];
		$this->user->birth_date = $data['birth_date'];
		$this->user->role = $data['role'];
		if ( $this->validation() )
		{
			R::store($this->user);
			return true;
		}else
			return false;

	}
}	