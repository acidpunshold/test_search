<?php
class Model_Signup extends Model
{
	public $username;
	public $password;
	public $name;
	public $lastname;
	public $birth_date;
	public $user;
	public $errors = array( 'username' => '', 'password' => '', 'name' => '', 'lastname' => '', 'birth_date' => '', 'wrong' => '' );

	public function validation()
	{
		$result = true;
		
		if (!empty( R::findOne('user', 'username = ?', array($this->username))) )
		{
			$this->errors['wrong'] = 'Пользователь с таким логином уже зарегестрирован';
			$result = false;
		}
		if ( empty($this->username) )
		{
			$this->errors['username'] = 'Введите логин';
			$result = false;
		}
		if ( empty($this->password) )
		{
			$this->errors['password'] = 'Введите пароль';
			$result = false;
		}
		if ( empty($this->name) )
		{
			$this->errors['name'] = 'Введите имя';
			$result = false;
		}
		if ( empty($this->lastname) )
		{
			$this->errors['lastname'] = 'Введите фамилию';
			$result = false;
		}
		if ( empty($this->birth_date) )
		{
			$this->errors['birth_date'] = 'Введите дату рождения';
			$result = false;
		}
		return $result;
	}

	public function signup()
	{

		$user = R::dispense('user');
		$user->username = $this->username;
		$user->password = sha1($this->password);
		$user->name = $this->name;
		$user->lastname = $this->lastname;
		$user->birth_date = $this->birth_date;
		R::store($user);
	}


}