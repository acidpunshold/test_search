<?php
class Model_Login extends Model
{
	public $username;
	public $password;
	public $user;
	public $errors = array( 'username' => '', 'password' => '', 'wrong' => '' );

	public function validation()
	{
		$result = true;
		if ( empty($this->username) )
		{
			$this->errors['username'] = 'Введите логин';
			$result = false;
		}
		if ( empty($this->password) )
		{
			$this->errors['password'] = 'Введите пароль';
			$result = false;
		}
		return $result;
	}

	public function login()
	{
		$result = false;
		$pass = sha1($this->password);

		
		if ( ($this->username == $this->user->username) && ($pass == $this->user->password) )
		{
			$_SESSION['logged_user'] = $this->user;
			$result = true;
			return $result;
		
		}else
		{		 
			return $result;
		}
	}
	static function logout()
	{
		unset($_SESSION['logged_user']);
		header('Location: /');
	}
}