<?php
require_once 'app/libs/simple_html_dom.php';

class Model_Search extends Model
{
	public $query;
  public $request;

  public function getSearchRequest()
  {
    $ch = curl_init();

    curl_setopt ($ch, CURLOPT_HEADER, 0);
    curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER , 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.0; en; rv:1.9.0.4) Gecko/2009011913 Firefox/3.0.6");
    $url = sprintf('http://www.google.com/search?q=%s', $this->query);
    curl_setopt ($ch, CURLOPT_URL, $url);
    $request = curl_exec ($ch);

    curl_close($ch);

    $request = str_replace('/url?q=', 'https://google.com/url?q=', $request);
    $request = str_get_html($request);

    $this->request = $request->find('div.g');
    return($this->request);
  }

}
