<h1>Вход</h1>
<?php if ( !empty ($data->errors['wrong']) ): ?>
	<div class="alert alert-warning alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  	<?= $data->errors['wrong'] ?>
	</div>
<?php endif ?>

<form action="login" method="post">
  	<div class="form-group">
    	<label for="username">Логин</label>
    	<input type="text" name="username" class="form-control" placeholder="Логин">
		<?php if ( !empty ($data->errors['username']) ): ?>
			<div class="alert alert-warning alert-dismissible" role="alert">
	  			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  			<?= $data->errors['username'] ?>
			</div>
		<?php endif ?>
  	</div>

  	<div class="form-group">
    	<label for="password">Пароль</label>
    	<input type="password" name="password" class="form-control" placeholder="Пароль">
		<?php if ( !empty ($data->errors['password']) ): ?>
    		<div class="alert alert-warning alert-dismissible" role="alert">
	  			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  			<?= $data->errors['password'] ?>
			</div>
		<?php endif ?>
  	</div>

  <button type="submit" class="btn btn-success">Войти</button>
</form>