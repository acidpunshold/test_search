<h1>Регистрация</h1>

<?php if ( !empty ($data->errors['wrong']) ): ?>
	<div class="alert alert-warning alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  	<?= $data->errors['wrong'] ?>
	</div>
<?php endif ?>

<form action="signup" method="post">
	<div class="form-group">
		<label for="username">Логин</label>
    	<input type="text" name="username" class="form-control" placeholder="agarik">
		<?php if ( !empty ($data->errors['username']) ): ?>
			<div class="alert alert-warning alert-dismissible" role="alert">
	  			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  			<?= $data->errors['username'] ?>
			</div>
		<?php endif ?>
  	</div>

  	<div class="form-group">
    	<label for="password">Пароль</label>
    	<input type="password" name="password" class="form-control" placeholder="Пароль">
		<?php if ( !empty ($data->errors['password']) ): ?>
    		<div class="alert alert-warning alert-dismissible" role="alert">
	  			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  			<?= $data->errors['password'] ?>
			</div>
		<?php endif ?>
  	</div>

  	<div class="form-group">
		<label for="name">Имя</label>
    	<input type="text" name="name" class="form-control" placeholder="Вечаслав">
		<?php if ( !empty ($data->errors['name']) ): ?>
			<div class="alert alert-warning alert-dismissible" role="alert">
	  			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  			<?= $data->errors['name'] ?>
			</div>
		<?php endif ?>
  	</div>

  	<div class="form-group">
		<label for="lastname">Фамилия</label>
    	<input type="text" name="lastname" class="form-control" placeholder="Огоньков">
		<?php if ( !empty ($data->errors['lastname']) ): ?>
			<div class="alert alert-warning alert-dismissible" role="alert">
	  			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  			<?= $data->errors['lastname'] ?>
			</div>
		<?php endif ?>
  	</div>

  	<div class="form-group">
		<label for="birth_date">Дата Рождения</label>
    	<input type="date" name="birth_date" class="form-control" >
		<?php if ( !empty ($data->errors['birth_date']) ): ?>
			<div class="alert alert-warning alert-dismissible" role="alert">
	  			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  			<?= $data->errors['birth_date'] ?>
			</div>
		<?php endif ?>
  	</div>

  <button type="submit" class="btn btn-success">Зарегестрироваться</button>
</form>