<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

	<title>Document</title>
	<!-- Последняя компиляция и сжатый CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Дополнение к теме -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Последняя компиляция и сжатый JavaScript -->

</head>
<body>
	<nav class="navbar navbar-inverse">
	  <div class="container">
	    <!-- Brand и toggle сгруппированы для лучшего отображения на мобильных дисплеях -->
	    <div class="navbar-header">
	      <a class="navbar-brand" href="/">Users&Search</a>
	    </div>

	    <!-- Соберите навигационные ссылки, формы, и другой контент для переключения -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="/">Главная</a></li>
		    <?php if ( empty($_SESSION['logged_user']) ): ?>
		        <li><a href="/login">Вход</a></li>
		        <li><a href="/signup">Регистрация</a></li>

		    <?php else: ?>
					<li><a href="/login/logout">Выход</a></li>
				<?php if ( $_SESSION['logged_user']->role == 'admin' ): ?>
		        	<li><a href="/admin/index">Админ панель</a></li>
		        <?php endif ?>
		    <?php endif ?>
					<li><a href="/search">Поиск</a></li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<div class="container">
		<?php include 'app/views/'.$content_view; ?>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script><script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.min.js"><\/script>')</script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
