<h1>Админка</h1>
<ul class="nav nav-tabs">
  <li role="presentation"><a href="/admin">Главная</a></li>
  <li role="presentation" class="active"><a href="/admin/users">Пользователи</a></li>
</ul>
<table class="table table-hover">
	<thead>
		<tr>
			<th>#</th>
			<th>Логин</th>
			<th>Имя</th>
			<th>Фамилия</th>
			<th>Дата рождения</th>
			<th>Права</th>
			<th>Действия</th>
		</tr>
	</thead>
	
	<tbody>	
		<?php foreach ($data->users as $user): ?>
			<tr>
				<th><?= $user['id'] ?></th>
				<th><a href="/admin/user/<?= $user['id'] ?>"><?= $user['username'] ?></a></th>
				<th><?= $user['name'] ?></th>
				<th><?= $user['lastname'] ?></th>
				<th><?= $user['birth_date'] ?></th>
				<th><?= $user['role'] ?></th>
				<th><a href="/admin/update/<?= $user['id'] ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>  <a href="/admin/delete/<?= $user['id'] ?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></th>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>

<nav aria-label="...">
  <ul class="pager">
    <li><a href="<?= $data->page - 1 ?>">Previous</a></li>
    <li><a href="<?= $data->page + 1 ?>">Next</a></li>
  </ul>
</nav>
