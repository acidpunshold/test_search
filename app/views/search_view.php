<h1>Поиск</h1>

<form action="" method="GET">
  	<div class="form-group">
    	<label for="query">Поисковый запрос</label>
    	<input type="text" name="query" class="form-control" value="<?= $data->query ?>" placeholder="Поисковый запрос">
		<?php if ( !empty ($data->errors['username']) ): ?>
			<div class="alert alert-warning alert-dismissible" role="alert">
	  			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  			<?= $data->errors['username'] ?>
			</div>
		<?php endif ?>
  	</div>


  <button type="submit" class="btn btn-success">Найти</button>
</form>

<?php if (!empty($data->request)): ?>
  <?php foreach ($data->request as $result): ?>
    <?= $result ?>
  <?php endforeach; ?>
<?php endif; ?>
