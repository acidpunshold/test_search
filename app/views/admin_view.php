<h1>Админка</h1>
<ul class="nav nav-tabs">
  <li role="presentation" class="active"><a href="/admin">Главная</a></li>
  <li role="presentation"><a href="/admin/users/">Пользователи</a></li>
</ul>

<p>Вы перешли в админ-панель. Здесь вы можете просматривать пользователей, редактировать их и удалять.</p>