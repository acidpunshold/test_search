<h1>Редактирование</h1>

<?php if ( !empty ($data->errors['wrong']) ): ?>
	<div class="alert alert-warning alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  	<?= $data->errors['wrong'] ?>
	</div>
<?php endif ?>

<form action="/admin/update/<?= $data->user->id ?>" method="post">
	<div class="form-group">
		<label for="username">Логин</label>
    	<input type="text" name="username" class="form-control" placeholder="agarik" value="<?= $data->user->username ?>">
		<?php if ( !empty ($data->errors['username']) ): ?>
			<div class="alert alert-warning alert-dismissible" role="alert">
	  			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  			<?= $data->errors['username'] ?>
			</div>
		<?php endif ?>
  	</div>

  	
  	<div class="form-group">
		<label for="name">Имя</label>
    	<input type="text" name="name" class="form-control" placeholder="Вечаслав" value="<?= $data->user->name ?>">
		<?php if ( !empty ($data->errors['name']) ): ?>
			<div class="alert alert-warning alert-dismissible" role="alert">
	  			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  			<?= $data->errors['name'] ?>
			</div>
		<?php endif ?>
  	</div>

  	<div class="form-group">
		<label for="lastname">Фамилия</label>
    	<input type="text" name="lastname" class="form-control" placeholder="Огоньков" value="<?= $data->user->lastname ?>">
		<?php if ( !empty ($data->errors['lastname']) ): ?>
			<div class="alert alert-warning alert-dismissible" role="alert">
	  			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  			<?= $data->errors['lastname'] ?>
			</div>
		<?php endif ?>
  	</div>

  	<div class="form-group">
		<label for="birth_date">Дата Рождения</label>
    	<input type="date" name="birth_date" class="form-control" value="<?= $data->user->birth_date ?>">
		<?php if ( !empty ($data->errors['birth_date']) ): ?>
			<div class="alert alert-warning alert-dismissible" role="alert">
	  			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  			<?= $data->errors['birth_date'] ?>
			</div>
		<?php endif ?>
  	</div>

  	<div class="form-group">
		<label for="role">Права</label>
		<select name="role" class="form-control">
			<option value="user">Пользователь</option>
			<option value="admin">Администратор</option>
		</select>

		
  	</div>

  <button type="submit" class="btn btn-success">Зарегестрироваться</button>
</form>