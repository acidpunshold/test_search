<h1>Страница пользователя <?= $data->username ?></h1>

<p><strong>Имя: </strong><?= $data->name ?></p>

<p><strong>Фамилия: </strong><?= $data->lastname ?></p>

<p><strong>Дата рождения: </strong><?= $data->birth_date ?></p>

<p><strong>Права: </strong><?= $data->role ?></p>

<a href="/admin/update/<?= $data->id ?>" class="btn btn-primary">Редактировать</a>

<a href="/admin/delete/<?= $data->id ?>" class="btn btn-danger">Удалить</a>